# -*- coding: utf-8 -*-
import time
import logging 
from datetime import datetime, timedelta
import json

from homeservio.plugin import Plugin
from homeservio.plugin.api import DeviceFactory, MetaFactory, register_device_change_request, send_device_update_event, Api

try:
    import td
    ENABLE = True
except OSError:
    ENABLE = False
    
logger = logging.getLogger()

class Tellstick(Plugin):
    
    def __init__(self, name, settings):
        self.grace_period = settings.get("sensor_grace_period", 180) # grace_period for sensors
        self.enabled = ENABLE
        self.name = name
        self.device_map = {}
        self.sensors = {}
        self.banned_sensors = []
        for k, v in settings.iteritems():
            if k.lower().startswith("device."):
                try:
                    self.device_map[int(k.split(".")[1])] = int(v)
                except (IndexError, ValueError):
                    logger.warning("Skipping illegal setting: %s = %s ", k, v)
        if settings.get("banned_sensors"):
            self.banned_sensors = ["{}.{}".format(self.name, x.strip()) for x in settings.get("banned_sensors").split(",")]
            
        if self.enabled:
            register_device_change_request(self.device_change_request)
            self.supported_methods = td.TELLSTICK_TURNON | td.TELLSTICK_TURNOFF | td.TELLSTICK_DIM
            # currently not supporting 
            #     TELLSTICK_BELL 
            #     TELLSTICK_TOGGLE 
            #     TELLSTICK_LEARN 
            #     TELLSTICK_EXECUTE 
            #     TELLSTICK_UP 
            #     TELLSTICK_DOWN 
            #     TELLSTICK_STOP 
    
    def run(self, name, settings):
        if not self.enabled:
            logger.error("%s disabled, could not load lib", name)
            return
        td.init()
        td.registerSensorEvent(self.tellstick_sensor_event)
        td.registerDeviceEvent(self.tellstick_device_event)
        td.registerDeviceChangedEvent(self.tellstick_device_changed_event)
        # td.registerRawDeviceEvent(self.tellstick_raw_device_event)
        # send all  devices known by telldusd, do not send sensors
        for i in range(td.getNumberOfDevices()):
            deviceId = td.getDeviceId(i)
            dev = self.getDevice(deviceId)
            self.send_device(deviceId, dev)
        # todo fix this nicer, eg api loop
        Api.instance().start()
        while 1:
            time.sleep(10)

    def device_change_request(self, address, msg):
        logger.debug("dcr %s: %s", address, msg[1])
        device = json.loads(msg[1])
        parts = address.split('.')
        if len(parts) != 2:
            return
        me, id_ = parts
        if me != self.name:
            return
        id_ = int(id_)
        if self.device_map.get(id_):
            id_ = self.device_map.get(id_)
        name = td.getName(id_)
        if device.get("name"):
            newname = device.get("name").encode("utf-8")
            oldname = td.getName(id_)
            if newname != oldname:
                logger.debug("changing name of %s to %s", oldname, newname )
                td.setName(id_, newname)
        if device.get("state") == "on":
            logger.debug("turning %s on", name)
            td.turnOn(id_)
        elif device.get("state") == "off":
            logger.debug("turning %s off", name)
            td.turnOff(id_)
        elif device.get("state") == "dim":
            if device.get("level") is not None:
                level = int(device.get("level"))
            else:
                last = self.getDevice(id_)
                level = last.level
            logger.debug("Dimming %s (%s)", name, level)
            td.dim(id_, ((level + 1) * 255/100) - 1)
        elif device.get("level") is not None:
            level = int(device.get("level"))
            logger.debug("Dimming %s (%s)", name, level)
            td.dim(id_, ((level + 1) * 255/100) - 1)

        
    def send_sensor(self, protocol, model, sensorId, dataType, value, timestamp):    
        if dataType == td.TELLSTICK_TEMPERATURE:
            unit = u"°C"
            gauge = "temperature"
        elif dataType == td.TELLSTICK_HUMIDITY:
            unit = u"% RH"
            gauge = "humidity"
        else:
            logger.error("illegal datatype (%s)",  dataType)
            unit = None
        address = "{}.{}.{}.{}".format(self.name, protocol, model, str(sensorId))
        if address in self.banned_sensors:
            logger.debug("Banning sensor %s", address)
            return
        sensor = self.sensors.get(sensorId)
        if sensor is None:
            sensor = DeviceFactory.create("{} {} {}".format(self.name, protocol, str(sensorId)))
            self.sensors[sensorId] = sensor
        if not hasattr(sensor, gauge):
            sensor.add_meta(gauge, MetaFactory.gauge(gauge, unit) )
        setattr(sensor, gauge, value)
        ctime = datetime.utcfromtimestamp(timestamp)
        grace = not (sensor.last_change and ctime - sensor.last_change <  timedelta(seconds = self.grace_period))
        sensor.last_change = datetime.utcfromtimestamp(timestamp)
        if grace:
            logger.info("Sensor %s within grace, not sending", address)
        else:
            logger.debug("Sending sensor %s %s", address, sensor.toJson())
            send_device_update_event(address, sensor.toJson())

    def tellstick_sensor_event(self, protocol, model, sensorId, dataType, value, timestamp, callbackId):
        self.send_sensor(protocol, model, sensorId, dataType, value, timestamp)
    
    def getDevice(self, deviceId):
        cmd = td.lastSentCommand(deviceId, methodsSupported = self.supported_methods, readable = True).lower()
        methods = td.methods(deviceId, methodsSupported = self.supported_methods, readable = False)
        name = td.getName(deviceId).decode("utf-8")
        if methods & td.TELLSTICK_DIM:
            try:
                level = int(td.lastSentValue(deviceId)) * 100 / 255
            except ValueError:
                level = 50
            
            dev = DeviceFactory.dimmer(name, cmd, level)
        else:
            dev = DeviceFactory.switch(name, cmd)
        return dev
            
    def send_device(self, deviceId, dev):
        if self.device_map.get(deviceId):
            deviceId = self.device_map.get(deviceId)
        address = "{}.{}".format(self.name, deviceId)
        logger.debug("Sending device %s %s", address, dev.toJson())
        send_device_update_event(address, dev.toJson())

    def tellstick_device_event(self, deviceId, method, data, callbackId):
        """
        The callback for device events.

        Parameters:
            deviceId    The id of the device that changed.
            method    The new device state. Can be TELLSTICK_TURNON, TELLSTICK_TURNOFF, etc.
            data    If method is TELLSTICK_DIM this holds the current value as a human readable string, example "128" for 50%.
            callbackId    The id of the callback.
            context    The pointer passed when registering for the event.
        """
        logger.debug("Device event method %s data %s", method, data)
        dev = DeviceFactory.create()
        if method == td.TELLSTICK_TURNON:
            dev.state = 'on'
        elif method == td.TELLSTICK_TURNOFF:
            dev.state = 'off'
        elif method == td.TELLSTICK_DIM:
            dev.state = 'dim'
            dev.level = int(data) * 100 / 255
        else:
            logger.warning("Unsupported method %s", method)
            return
        logger.debug("device event <%s>", str(dev))
        dev.last_change = datetime.utcnow()
        self.send_device(deviceId, dev)
            
    def tellstick_device_changed_event(self, deviceId, changeEvent, changeType, callbackId):
        """
        The callback for device change events.

        Attention: The callback will be called by another thread than the thread used by the application and some measures must be taken to synchronize it with the main thread.
        Parameters: 
            deviceId    The id of the device that was added, changed or removed.
            changeEvent    One of the constants TELLSTICK_DEVICE_ADDED, TELLSTICK_DEVICE_CHANGED or TELLSTICK_DEVICE_REMOVED.
            changeType    If changeEvent is TELLSTICK_DEVICE_CHANGED, this parameter indicates what has changed (e.g TELLSTICK_CHANGE_NAME, TELLSTICK_CHANGE_PROTOCOL, TELLSTICK_CHANGE_MODEL or TELLSTICK_CHANGE_METHOD).
            callbackId    The id of the callback.
        """
        dev = self.getDevice(deviceId)
        logger.debug("Device changed event <%s>", str(dev))
        dev.last_changed = datetime.utcnow()
        # TODO we should not send in another thread, fix this in api
        self.send_device(deviceId, dev)
    
    def tellstick_raw_device_event(self, data, controllerId, callbackId):
        logger.info("raw device event %s %s", data, controllerId)
        # todo  support buttons  
        
    
    
