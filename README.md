# Homeservio tellstick

A TellStick Duo adapter for homeservio homeserver. The tellstick adapter supports [TellStick Duo](http://www.telldus.se/products/tellstick_duo).

For more information see https://homeservio.com/plugins/homeservio-tellstick

## License information

    Copyright (C) 2014 homeservio.com. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Supported platforms

Currently tested on Linux/debian with Python 2.7.

## Release notes

### 0.3.4
- Sensor grace period added to remove unwanted sensors

### 0.3.3
- Name issue

### 0.3.2
- Dimmer fault fixed

### 0.3.0
- Sensors from same physical device now in same gauge in live
- Possibility to ban sensors added


### 0.2.0
- Added support for tdtool.py

### 0.1.3
- First alfa release