Homeservio tellstick
====================

A TellStick Duo adapter for homeservio homeserver. The tellstick adapter
supports `TellStick
Duo <http://www.telldus.se/products/tellstick_duo>`__.

Installation
------------

The tellstick adapter can be installed via

::

    homeserver install tellstick

or

::

    pip install homeservio-tellstick -i https://homeservio.com/simple

Prerequeistes
~~~~~~~~~~~~~

telldus-core
^^^^^^^^^^^^

Before installing, telldus-core must be installed on the homeserver and
the TellStick Duo must be plugged in to a USB interface.

To install telldus-core, follow the instructions in `Embedded Linux
Wiki <http://elinux.org/R-Pi_Tellstick_core>`__

Homeservio homeserver
^^^^^^^^^^^^^^^^^^^^^

The other prerequisite is the homeservio homeserver. Visit
https://homeservio.com for more information on how to install the
homeserver.

Configuration
-------------

Homeservio-tellstick is configured in the ``[plugin:tellstick]`` section
of the homeserver configuration file.

disable
~~~~~~~

If disabled is set, the plugin do not start automatically at startup of
homeserver.

Example:

::

    [plugin:tellstick]
    disable

loglevel
~~~~~~~~

Default loglevel is info, giving normal amount of messages in the log
file. You can change this to

error - only errors will be written in the log file warning - errors and
warnings will be written in the log file info - informational messages
will be written in the log file, as well as errors and warnings debug -
as above plus that debug messages will be written

Note that this overrides same setting on the [HOMESERVER] level in the
configuration for the tellstick plugin.

Example:

::

    [plugin:tellstick]
    loglevel = debug   

several buttons controlling one switch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to have several switches controlling same lamp (switch or
dimmer) you can add them in the configuration.

Example:

::

    [plugin:tellstick]
    device.1 = 4
    device.3 = 5
    device.6 = 5

In the above example, device 1 and 4 controls the same switch or dimmer.
Device 4 is the "master". Device 3, 6 and 5 controls the same switch or
dimmer. In *live* only switch 4 and 5 will be visible.

banning sensors
~~~~~~~~~~~~~~~

There are several reasons why the tellstick sometimes get bad sensor
values. It could be your neighbours sensor or it could a distrubance, or
even a button can emit something that the tellstick thinks is sensor
values.

sensor\_grace\_period
^^^^^^^^^^^^^^^^^^^^^

Default: 180 (seconds)

Sensors are not updated within the grace period. Two consecutive updates
must come within this time period for an update to occur.

To change grace period, add sensor\_grace\_period to your configuration.
You can set sensor\_grace\_period to 0 to disable this functionality.

Example:

::

    [plugin:tellstick]
    sensor_grace_period = 360

banned\_sensors
^^^^^^^^^^^^^^^

In order to ban thoose faulty sensors, add them in the
``banned_sensors`` list. The name to ban is the address in live (minus
tellstick) or the same as is output from ´tdtool.py -l´

Example:

::

    $ tdtool.py -l
    Number of devices: 10
    ...
    Number of sensors: 21
    Sensor: fineoffset.temperature.255               Temperature:       -204.7 2014-08-31 13:04:18
    Sensor: mandolyn.temperaturehumidity.14          Temperature:         22.6 2014-08-31 13:03:54
    Sensor: mandolyn.temperaturehumidity.14          Humidity   :           61 2014-08-31 13:03:54
    Sensor: fineoffset.temperaturehumidity.255       Temperature:       -153.6 2014-08-31 03:41:04

In the above example we obviously get bad data from
fineoffset.temperature.255 and fineoffset.temperaturehumidity.255. To
avoid them in the future add the following to your config file:

::

    [plugin:tellstick]
    banned_sensors = fineoffset.temperature.255, fineoffset.temperaturehumidity.255

*Note: after banning a sensor and restarting homeserver or the plugin,
you have to manually remove the sensors from live.*

tdtool.py
---------

In the package a tool to configure and test your tellstick configuration
is included. It's similar to tdtool from telldus with some extensions.
For more information, use ``tdtool.py --help``

::

    Options:
      -h, --help            show this help message and exit
      -l, --list            List currently configured devices and detected sensors.
      -n device, --on=device
                            Turns on device. 'device' could either be an integer
                            of the device-id, or the name of the device. Both
                            device-id and name is outputed with the --list option.
      -f device, --off=device
                            Turns off device. 'device' could either be an integer
                            of the device-id, or the name of the device. Both
                            device-id and name is outputed with the --list option.
      -d device, --dim=device
                            Dim device. 'device' could either be an integer of the
                            device-id, or the name of the device. Both device-id
                            and name is outputed with the --list option. Note: The
                            dimlevel parameter must also be set on the commandline
      -v level, --dimlevel=level
                            Set dim level. 'level' should be an integer, 0-255.
                            Note: This parameter also requires the --dim/-d for
                            anything to happen.
      -b device, --bell=device
                            Sends bell command to devices supporting this.
                            'device' could either be an integer of the device-id,
                            or the name of the device. Both device-id and name is
                            outputed with the --list option.
      -e device, --learn=device
                            Sends a special learn command to devices supporting
                            this. This is normaly devices of 'selflearning' type.
                            'device' could either be an integer of the device-id,
                            or the name of the device. Both device-id and name is
                            outputed with the --list option.
      -t, --event           Listen for events untill interrupted by ctrl-c

Supported devices
-----------------

All devices that are supported by the TellStick Duo and telldus-core
should be supported by this plugin. However there are currently some
limitiations:

Devices relying on the following signals are currently only partly
supported:

-  TELLSTICK\_BELL
-  TELLSTICK\_TOGGLE
-  TELLSTICK\_LEARN
-  TELLSTICK\_EXECUTE
-  TELLSTICK\_UP
-  TELLSTICK\_DOWN
-  TELLSTICK\_STOP

Sensors
~~~~~~~

Sensors are gauges in homeserver. Currently the following sensors are
supported:

-  TELLSTICK\_TEMPERATURE
-  TELLSTICK\_HUMIDITY

The following types are currently not supported:

-  TELLSTICK\_RAINRATE
-  TELLSTICK\_RAINTOTAL
-  TELLSTICK\_WINDDIRECTION
-  TELLSTICK\_WINDAVERAGE
-  TELLSTICK\_WINDGUST

License information
-------------------

::

    Copyright (C) 2014 homeservio.com. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Supported platforms
-------------------

Currently tested on Linux/debian with Python 2.7.

Release notes
-------------

0.3.4
~~~~~

-  Sensor grace period added to remove unwanted sensors

0.3.3
~~~~~

-  Name issue

0.3.2
~~~~~

-  Dimmer fault fixed

0.3.0
~~~~~

-  Sensors from same physical device now in same gauge in live
-  Possibility to ban sensors added

0.2.0
~~~~~

-  Added support for tdtool.py

0.1.3
~~~~~

-  First alfa release

Info
====

-  Homepage: https://homeservio.com

